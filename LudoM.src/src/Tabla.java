import strategy.jugador.Player;
import strategy.jugador.Player_1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;


public class Tabla extends JFrame implements ActionListener {
  private JPanel tablero2;
    //private Panel fondo;
    JLabel sh1,sh2,sh3,sh4,sh5,sh6,sh7,sh8,st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11,st12,st13,st14,st15,st16;
    JButton movimiento;
    int dadito;
    public Tabla(){

        this.setLayout(null);
        setTitle("Ludo Matic");
        setLocationRelativeTo(null);
        this.setResizable(false);
        setMinimumSize(new Dimension(200,200));
        this.setBounds(0, 0, 800, 800);
        //setSize(800,800);
        this.getContentPane().setBackground(Color.darkGray);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.tablero2.setBounds(0, 0, 500, 500);
        this.tablero2.setBackground(new Color(190, 190, 190));
        this.tablero2.setVisible(false);
        this.add(tablero2);
        movimiento= new JButton("dado");

        JPanel[][] tablero1 = new JPanel[15][15];
    for(int i=0;i<15;i++) {
        for (int j = 0; j < 15; j++) {
            tablero1[i][j] = new JPanel();
            tablero1[i][j].setBounds((i + 1) * 40, (j + 1) * 40, 40, 40);
            this.add(tablero1[i][j]);
            if ((i <= 5 && j <= 5) || (i == 1 && j == 6) || ((i > 0 && i < 7) && j == 7)) {
                tablero1[i][j].setBackground(new Color(3, 94, 23));
            } else if ((i >= 9 && j <= 5) || (i == 8 && j == 1) || ((j > 0 && j < 7) && i == 7)) {
                tablero1[i][j].setBackground(new Color(217, 4, 4));
            } else if ((i <= 5 && j >= 9) || (i == 6 && j == 13) || ((j > 7 && j < 14) && i == 7)) {
                tablero1[i][j].setBackground(new Color(232, 210, 3));
            } else if ((i >= 9 && j >= 9) || (i == 13 && j == 8) || ((i > 7 && i < 14) && j == 7)) {
                tablero1[i][j].setBackground(new Color(3, 41, 232));
            } else {
                tablero1[i][j].setBackground(new Color(253, 253, 253));
            }

            tablero1[i][j].setBorder(BorderFactory.createLineBorder(Color.BLACK));
        }
    }
        //for(int i=0;i<15;i++) {
            //for (int j = 0; j < 15; j++){




   /* fondo = new Panel();
    tablero1[1][8].add(fondo);
    tablero1[1][8].setOpaque(true);*/

        movimiento.setIcon(new ImageIcon("Imagenes\\dado1.png"));
        movimiento.setBounds(674, 100, 85, 85);
       this.add(movimiento);
       movimiento.addActionListener(this);

        sh1 = new JLabel(); //verde 2
        sh1.setIcon(new ImageIcon("Imagenes\\shield2.1.jpg"));
        tablero1[2][8].setOpaque(true);
        sh1.setBounds(getX(),getY(),getWidth(),getHeight());
        tablero1[2][8].add(sh1);
        tablero1[2][8].repaint();
        tablero1[2][8].revalidate();
        this.repaint();
        this.revalidate();

        sh2 = new JLabel(); //rojo 2
        sh2.setIcon( new ImageIcon("Imagenes\\shield2.1.jpg"));
        tablero1[6][2].setOpaque(true);
        tablero1[6][2].add(sh2);
        tablero1[6][2].repaint();
        tablero1[6][2].revalidate();
        this.repaint();
        this.revalidate();


        sh3 = new JLabel(); //verde salida
        sh3.setIcon( new ImageIcon("Imagenes\\shield2.1.jpg"));
        tablero1[1][6].setOpaque(true);
        tablero1[1][6].add(sh3);
        tablero1[1][6].repaint();
        tablero1[1][6].revalidate();
        this.repaint();
        this.revalidate();


        sh4 = new JLabel(); //rojo salida
        sh4.setIcon( new ImageIcon("Imagenes\\shield2.1.jpg"));
        tablero1[8][1].setOpaque(true);
        tablero1[8][1].add(sh4);
        tablero1[8][1].repaint();
        tablero1[8][1].revalidate();
        this.repaint();
        this.revalidate();


        sh5 = new JLabel();//azul salida
        sh5.setIcon( new ImageIcon("Imagenes\\shield2.1.jpg"));
        tablero1[13][8].setOpaque(true);
        tablero1[13][8].add(sh5);
        tablero1[13][8].repaint();
        tablero1[13][8].revalidate();
        this.repaint();
        this.revalidate();

        sh6 = new JLabel(); //amarillo 2
        sh6.setIcon( new ImageIcon("Imagenes\\yellow.JPG"));
        tablero1[8][12].setOpaque(true);
        tablero1[8][12].add(sh6);
        tablero1[8][12].repaint();
        tablero1[8][12].revalidate();
        this.repaint();
        this.revalidate();

        sh7 = new JLabel(); //azul 2
        sh7.setIcon( new ImageIcon("Imagenes\\shield2.1.jpg"));
        tablero1[12][6].setOpaque(true);
        tablero1[12][6].add(sh7);
        tablero1[12][6].repaint();
        tablero1[12][6].revalidate();
        this.repaint();
        this.revalidate();

        sh8 = new JLabel(); //amarillo salida
        sh8.setIcon( new ImageIcon("Imagenes\\shield2.1.jpg"));
        tablero1[6][13].setOpaque(true);
        tablero1[6][13].add(sh8);
        tablero1[6][13].repaint();
        tablero1[6][13].revalidate();
        this.repaint();
        this.revalidate();

        //FICHAS AZULES

        st1= new JLabel(); //FICHA AZUL 1
        st1.setIcon(new ImageIcon("Imagenes\\blue4.JPG"));
        tablero1[11][11].add(st1);

        st2= new JLabel(); //FICHA AZUL 2
        st2.setIcon(new ImageIcon("Imagenes\\blue4.JPG"));
        tablero1[11][13].add(st2);

        st3= new JLabel(); //FICHA AZUL 3
        st3.setIcon(new ImageIcon("Imagenes\\blue4.JPG"));
        tablero1[13][13].add(st3);

        st4= new JLabel(); //FICHA AZUL 4
        st4.setIcon(new ImageIcon("Imagenes\\blue4.JPG"));
        tablero1[13][11].add(st4);

        //FICHAS AMARILLAS
        st5= new JLabel(); //FICHA AMARILLA 1
        st5.setIcon(new ImageIcon("Imagenes\\yellow.JPG"));
        tablero1[1][11].add(st5);

        st6= new JLabel(); //FICHA AMARILLA 2
        st6.setIcon(new ImageIcon("Imagenes\\yellow.JPG"));
        tablero1[1][13].add(st6);

        st7= new JLabel(); //FICHA AMARILLA 3
        st7.setIcon(new ImageIcon("Imagenes\\yellow.JPG"));
        tablero1[3][13].add(st7);

        st8= new JLabel(); //FICHA AMARILLA 4
        st8.setIcon(new ImageIcon("Imagenes\\yellow.JPG"));
        tablero1[3][11].add(st8);

        //FICHAS VERDES
        st9=new JLabel(); //FICHA VERDE 1
        st9.setIcon(new ImageIcon("Imagenes\\green.JPG"));
        tablero1[1][3].add(st9);

        st10= new JLabel(); //FICHA VERDE 2
        st10.setIcon(new ImageIcon("Imagenes\\green.JPG"));
        tablero1[1][1].add(st10);

        st11= new JLabel(); //FICHA VERDE 3
        st11.setIcon(new ImageIcon("Imagenes\\green.JPG"));
        tablero1[3][1].add(st11);

        st12= new JLabel(); //FICHA VERDE 4
        st12.setIcon(new ImageIcon("Imagenes\\green.JPG"));
        tablero1[3][3].add(st12);

        //FICHAS ROJAS
        st13= new JLabel(); //FICHA ROJA 1
        st13.setIcon(new ImageIcon("Imagenes\\red.JPG"));
        tablero1[11][3].add(st13);

        st14= new JLabel(); //FICHA ROJA 2
        st14.setIcon(new ImageIcon("Imagenes\\red.JPG"));
        tablero1[11][1].add(st14);

        st15= new JLabel(); //FICHA ROJA 3
        st15.setIcon(new ImageIcon("Imagenes\\red.JPG"));
        tablero1[13][1].add(st15);

        st16= new JLabel(); //FICHA ROJA 4
        st16.setIcon(new ImageIcon("Imagenes\\red.JPG"));
        tablero1[13][3].add(st16);



        /*jLabel1 = new JLabel();
        Image img= new ImageIcon("C:\\Users\\Maximiliano\\Google Drive\\Facu\\Ing de software\\TestigosDeTuring\\Imagenes\\shield.png").getImage();
        ImageIcon img2=new ImageIcon(img.getScaledInstance(tablero1[1][8].getWidth(), tablero1[1][8].getHeight(), Image.SCALE_SMOOTH));
        jLabel1.setIcon(img2);
        tablero1[1][8].add(jLabel1);*/




    }


    public void actionPerformed(ActionEvent e) {
       if(e.getSource()==movimiento){
           dadito=(int)(Math.random()*6 + 1);
           JOptionPane.showMessageDialog(null, dadito);
       }
    }

}